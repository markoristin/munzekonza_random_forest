include Makefile.inc
########################################################################################################################
PACKAGE=$(BUILD)/+munzekonza
PACKAGE_TEST=$(BUILD)/+munzekonza/+test

MKFILEPATH=$(abspath $(lastword $(MAKEFILE_LIST)))
CURRENT=$(patsubst %/,%,$(dir $(MKFILEPATH)))

CCFLAGS=-DMX_COMPAT_32 -D_GNU_SOURCE -ansi -fexceptions -fPIC -fno-omit-frame-pointer -pthread -DNDEBUG -O2 -std=c++11 \
	-I$(BOOST)/include -I$(EIGEN)/include/eigen3 -I$(CURRENT)/src/cpp -iquote$(CURRENT)/src/cpp -I$(MATLAB)/extern/include -I$(MATLAB)/simulink/include \
	-L$(BOOST)/lib -L$(MATLAB)/bin/glnxa64 -L$(BUILD)

MEXFLAGS=CXXFLAGS='$$CXXFLAGS -std=c++11 -O2' -I$(BOOST)/include -I$(EIGEN)/include/eigen3 -I$(CURRENT)/src/cpp -L$(BOOST)/lib -L$(BUILD)
########################################################################################################################
$(PACKAGE):
	mkdir -p $(PACKAGE)

$(PACKAGE_TEST):
	mkdir -p $(PACKAGE_TEST)

$(BUILD):
	mkdir -p $(BUILD)
########################################################################################################################
MUNZEKONZA_PARSING_SOURCES=src/cpp/munzekonza/parsing/exception.cpp \
			   src/cpp/munzekonza/parsing/not_readable_exception.cpp \
			   src/cpp/munzekonza/parsing/reader.cpp 

$(BUILD)/libmunzekonza_parsing.so: | $(BUILD)
	$(CC) $(CCFLAGS) -shared -Wl,-soname,libmunzekonza_parsing.so -o $(BUILD)/libmunzekonza_parsing.so \
	  $(MUNZEKONZA_PARSING_SOURCES) 

munzekonza_parsing: $(BUILD)/libmunzekonza_parsing.so
########################################################################################################################
MUNZEKONZA_NUMERIC_SOURCES=src/cpp/munzekonza/numeric/greedy_tsp.cpp

$(BUILD)/libmunzekonza_numeric.so: $(MUNZEKONZA_NUMERIC_SOURCES)  | $(BUILD) 
	$(CC) $(CCFLAGS) -shared -Wl,-soname,libmunzekonza_numeric.so -o $(BUILD)/libmunzekonza_numeric.so \
	  $(MUNZEKONZA_NUMERIC_SOURCES)

munzekonza_numeric: $(BUILD)/libmunzekonza_numeric.so
########################################################################################################################
$(BUILD)/libmunzekonza_debugging.so: src/cpp/munzekonza/debugging/debugging.cpp  | $(BUILD) 
	$(CC) $(CCFLAGS) -shared -Wl,-soname,libmunzekonza_debugging.so -o $(BUILD)/libmunzekonza_debugging.so \
	  src/cpp/munzekonza/debugging/debugging.cpp 
munzekonza_debugging: $(BUILD)/libmunzekonza_debugging.so
########################################################################################################################
MUNZEKONZA_UTILS_SOURCES=src/cpp/munzekonza/utils/eta.cpp \
			 src/cpp/munzekonza/utils/profiler.cpp \
			 src/cpp/munzekonza/utils/memory_usage.cpp \
			 src/cpp/munzekonza/utils/stl_operations.cpp \
			 src/cpp/munzekonza/utils/tictoc.cpp 

$(BUILD)/libmunzekonza_utils.so:  $(MUNZEKONZA_UTILS_SOURCES) | $(BUILD) 
	$(CC) $(CCFLAGS) -shared -Wl,-soname,libmunzekonza_utils.so -o $(BUILD)/libmunzekonza_utils.so \
	  $(MUNZEKONZA_UTILS_SOURCES) 

munzekonza_utils: $(BUILD)/libmunzekonza_utils.so
########################################################################################################################
MUNZEKONZA_RANDOM_FOREST_SOURCES=src/cpp/munzekonza/random_forest/leaf_stats.cpp \
				 src/cpp/munzekonza/random_forest/leaf_stats_collector.cpp \
				 src/cpp/munzekonza/random_forest/node.cpp \
				 src/cpp/munzekonza/random_forest/partition_criterions.cpp \
				 src/cpp/munzekonza/random_forest/entropier.cpp \
				 src/cpp/munzekonza/random_forest/seeper_abstract.cpp \
				 src/cpp/munzekonza/random_forest/tree_training.cpp \
				 src/cpp/munzekonza/random_forest/autosave.cpp \
				 src/cpp/munzekonza/random_forest/autosave_frequent.cpp

$(BUILD)/libmunzekonza_random_forest.so:  $(MUNZEKONZA_RANDOM_FOREST_SOURCES) | $(BUILD) 
	$(CC) $(CCFLAGS) -shared -Wl,-soname,libmunzekonza_random_forest.so -o $(BUILD)/libmunzekonza_random_forest.so \
	  $(MUNZEKONZA_RANDOM_FOREST_SOURCES) 

munzekonza_random_forest: $(BUILD)/libmunzekonza_random_forest.so
########################################################################################################################
MUNZEKONZA_NCM_FOREST_SOURCES=\
			      src/cpp/munzekonza/ncm_forest/split.cpp \
			      src/cpp/munzekonza/ncm_forest/partition_functions.cpp \
			      src/cpp/munzekonza/ncm_forest/partition_iterator.cpp \
			      src/cpp/munzekonza/ncm_forest/partitioner.cpp \
			      src/cpp/munzekonza/ncm_forest/seeper.cpp \
			      src/cpp/munzekonza/ncm_forest/training.cpp \
			      src/cpp/munzekonza/ncm_forest/training_cvpr14.cpp \
			      src/cpp/munzekonza/ncm_forest/training_regula.cpp \
			      src/cpp/munzekonza/ncm_forest/cast_splits.cpp

$(BUILD)/libmunzekonza_ncm_forest.so:  $(MUNZEKONZA_NCM_FOREST_SOURCES)  | $(BUILD) 
	$(CC) $(CCFLAGS) -shared -Wl,-soname,libmunzekonza_ncm_forest.so -o $(BUILD)/libmunzekonza_ncm_forest.so \
	  $(MUNZEKONZA_NCM_FOREST_SOURCES) 

munzekonza_ncm_forest: $(BUILD)/libmunzekonza_ncm_forest.so
########################################################################################################################
MUNZEKONZA_MEX_SOURCES=src/cpp/munzekonza/mex/mexout.cpp \
		       src/cpp/munzekonza/mex/base_array.cpp \
		       src/cpp/munzekonza/mex/array.cpp \
		       src/cpp/munzekonza/mex/const_array.cpp \
		       src/cpp/munzekonza/mex/struct.cpp 

$(BUILD)/libmunzekonza_mex.so:  $(MUNZEKONZA_MEX_SOURCES) | $(BUILD) 
	$(CC) $(CCFLAGS) -shared -Wl,-soname,libmunzekonza_mex.so -o $(BUILD)/libmunzekonza_mex.so \
	  $(MUNZEKONZA_MEX_SOURCES) 

munzekonza_mex: $(BUILD)/libmunzekonza_mex.so
########################################################################################################################
munzekonza:  munzekonza_parsing \
  munzekonza_debugging  \
  munzekonza_utils  \
  munzekonza_numeric  \
  munzekonza_mex \
  munzekonza_random_forest \
  munzekonza_ncm_forest \

MUNZEKONZA_LFLAGS=-lmunzekonza_parsing \
		  -lmunzekonza_debugging \
		  -lmunzekonza_utils \
		  -lmunzekonza_mex \
		  -lmunzekonza_random_forest \
		  -lmunzekonza_ncm_forest \
		  -lmunzekonza_numeric \
		  -lboost_system \
		  -lboost_filesystem \
		  -lboost_regex
########################################################################################################################
$(PACKAGE)/train_ncm_tree.mexa64: munzekonza src/cpp/munzekonza/ncm_forest/procedures/train_ncm_tree.cpp | $(PACKAGE)
	$(MEX) $(MEXFLAGS) \
	  $(MUNZEKONZA_LFLAGS) \
	 src/cpp/munzekonza/ncm_forest/procedures/train_ncm_tree.cpp \
	 -output $(PACKAGE)/train_ncm_tree
########################################################################################################################
$(PACKAGE)/populate_leaf_stats_of_ncm_tree.mexa64: munzekonza src/cpp/munzekonza/ncm_forest/procedures/populate_leaf_stats_of_ncm_tree.cpp | $(PACKAGE)
	$(MEX) $(MEXFLAGS) \
	  $(MUNZEKONZA_LFLAGS) \
	 src/cpp/munzekonza/ncm_forest/procedures/populate_leaf_stats_of_ncm_tree.cpp \
	 -output $(PACKAGE)/populate_leaf_stats_of_ncm_tree
########################################################################################################################
$(PACKAGE)/apply_ncm_tree.mexa64: munzekonza src/cpp/munzekonza/ncm_forest/procedures/apply_ncm_tree.cpp | $(PACKAGE)
	$(MEX) $(MEXFLAGS) \
	  $(MUNZEKONZA_LFLAGS) \
	 src/cpp/munzekonza/ncm_forest/procedures/apply_ncm_tree.cpp \
	 -output $(PACKAGE)/apply_ncm_tree
########################################################################################################################
$(PACKAGE)/load_tree.mexa64: munzekonza src/cpp/munzekonza/ncm_forest/procedures/load_tree.cpp | $(PACKAGE)
	$(MEX) $(MEXFLAGS) \
	  $(MUNZEKONZA_LFLAGS) \
	  -lboost_serialization -lboost_filesystem -lboost_system \
	 src/cpp/munzekonza/ncm_forest/procedures/load_tree.cpp \
	 -output $(PACKAGE)/load_tree
########################################################################################################################
$(PACKAGE)/load_ncm_splits.mexa64: munzekonza src/cpp/munzekonza/ncm_forest/procedures/load_ncm_splits.cpp | $(PACKAGE)
	$(MEX) $(MEXFLAGS) \
	  $(MUNZEKONZA_LFLAGS) \
	  -lboost_serialization -lboost_filesystem -lboost_system \
	 src/cpp/munzekonza/ncm_forest/procedures/load_ncm_splits.cpp \
	 -output $(PACKAGE)/load_ncm_splits
########################################################################################################################
$(PACKAGE)/save_tree.mexa64: munzekonza src/cpp/munzekonza/ncm_forest/procedures/save_tree.cpp | $(PACKAGE)
	$(MEX) $(MEXFLAGS) \
	  $(MUNZEKONZA_LFLAGS) \
	  -lboost_serialization -lboost_filesystem -lboost_system \
	 src/cpp/munzekonza/ncm_forest/procedures/save_tree.cpp \
	 -output $(PACKAGE)/save_tree
########################################################################################################################
$(PACKAGE)/save_ncm_splits.mexa64: munzekonza src/cpp/munzekonza/ncm_forest/procedures/save_ncm_splits.cpp | $(PACKAGE)
	$(MEX) $(MEXFLAGS) \
	  $(MUNZEKONZA_LFLAGS) \
	  -lboost_serialization -lboost_filesystem -lboost_system \
	 src/cpp/munzekonza/ncm_forest/procedures/save_ncm_splits.cpp \
	 -output $(PACKAGE)/save_ncm_splits
########################################################################################################################
$(PACKAGE)/destroy_tree.mexa64: munzekonza src/cpp/munzekonza/ncm_forest/procedures/destroy_tree.cpp | $(PACKAGE)
	$(MEX) $(MEXFLAGS) $(MUNZEKONZA_LFLAGS) \
	 src/cpp/munzekonza/ncm_forest/procedures/destroy_tree.cpp \
	 -output $(PACKAGE)/destroy_tree
########################################################################################################################
$(PACKAGE)/destroy_ncm_splits.mexa64: munzekonza src/cpp/munzekonza/ncm_forest/procedures/destroy_ncm_splits.cpp | $(PACKAGE)
	$(MEX) $(MEXFLAGS) $(MUNZEKONZA_LFLAGS) \
	 src/cpp/munzekonza/ncm_forest/procedures/destroy_ncm_splits.cpp \
	 -output $(PACKAGE)/destroy_ncm_splits
########################################################################################################################
all: \
  $(PACKAGE)/train_ncm_tree.mexa64 \
  $(PACKAGE)/populate_leaf_stats_of_ncm_tree.mexa64 \
  $(PACKAGE)/apply_ncm_tree.mexa64 \
  $(PACKAGE)/load_tree.mexa64 \
  $(PACKAGE)/load_ncm_splits.mexa64 \
  $(PACKAGE)/save_tree.mexa64 \
  $(PACKAGE)/save_ncm_splits.mexa64 \
  $(PACKAGE)/destroy_tree.mexa64 \
  $(PACKAGE)/destroy_ncm_splits.mexa64 
########################################################################################################################
# tests
$(PACKAGE_TEST)/run_tests.m: src/matlab/+munzekonza/+test/run_tests.m | $(PACKAGE_TEST) 
	cp src/matlab/+munzekonza/+test/run_tests.m $(PACKAGE_TEST)/
	
$(PACKAGE_TEST)/test_linking.mexa64: munzekonza \
  src/cpp/munzekonza/ncm_forest/tests/test_linking.cpp | $(PACKAGE_TEST)
	$(MEX) $(MEXFLAGS) $(MUNZEKONZA_LFLAGS) \
	 src/cpp/munzekonza/ncm_forest/tests/test_linking.cpp  \
	 -output $(PACKAGE_TEST)/test_linking

tests: $(PACKAGE_TEST)/run_tests.m \
  $(PACKAGE_TEST)/test_linking.mexa64
########################################################################################################################
clean:
	rm -rf $(BUILD)/*.so $(PACKAGE_TEST)/*.mexa64 $(PACKAGE)/*.mexa64 
