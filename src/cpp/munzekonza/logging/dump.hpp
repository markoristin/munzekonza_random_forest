//
// File:          munzekonza/logging/dump.hpp
// Author:        Marko Ristin
// Creation date: Jul 22 2014
//

#ifndef MUNZEKONZA_LOGGING_DUMP_HPP_
#define MUNZEKONZA_LOGGING_DUMP_HPP_

#include <iostream>
#include <string>
#include <sstream>
#include <ostream>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <string>

namespace munzekonza {
namespace logging {

//
// out definitions
//
template<typename T> void out( std::ostream& o, const T& x );

void out( std::ostream& o, const std::string& x );
void out( std::ostream& o, const char* x );

template<typename T> void out( std::ostream& o, const std::vector<T>& x );
template<typename T> void out( std::ostream& o, const std::set<T>& x );
template<typename T> void out( std::ostream& o, const std::list<T>& x );
template<typename K, typename V> void out( std::ostream& o, const std::map<K, V>& x );

//
// out implementations
//
template<typename T>
void out( std::ostream& o, const std::vector<T>& x ) {
  o << "std::vector({";
  for( int i = 0; i < x.size(); ++i ) {
    out( o, x[i] );
    if( i < x.size() - 1 ) {
      o << ", ";
    }
  }
  o << "})";
}

template<typename T> void out( std::ostream& o, const std::set<T>& x ) {
  const int n = x.size();
  int i = 0;
  o << "std::set({";
  for( auto it = x.begin(); it != x.end(); ++it ) {
    o << *it;
    if( i < n - 1 ) {
      o << ", ";
    }
    ++i;
  }
  o << "})";
}

template<typename T> void out( std::ostream& o, const std::list<T>& x ) {
  const int n = x.size();
  int i = 0;
  o << "std::list({";
  for( auto it = x.begin(); it != x.end(); ++it ) {
    out( o, *it );

    if( i < n - 1 ) {
      o << ", ";
    }
    ++i;
  }
  o << "})";
}

template<typename K, typename V> void out( std::ostream& o, const std::map<K, V>& x ) {
  const int n = x.size();
  int i = 0;
  o << "std::map({";
  for( auto it = x.begin(); it != x.end(); ++it ) {
    o << "{";
    out( o, it->first );
    o << ", ";
    out( o, it->second );
    o << "}";

    if( i < n - 1 ) {
      o << ", ";
    }
    ++i;
  }
  o << "})";
}

void out( std::ostream& o, const char* x ) {
  std::string x_str( x );
  out( o, x_str );
}

void out( std::ostream& o, const std::string& x ) {
  std::string escaped;
  escaped.reserve( 2 + 2 * x.size() );
  escaped.push_back( '"' );

  for( int i = 0; i < x.size(); ++i ) {
    if( x[i] == '\\' ) {
      escaped.push_back( '\\' );
      escaped.push_back( '\\' );
    } else if( x[i] == '\n' ) {
      escaped.push_back( '\\' );
      escaped.push_back( 'n' );
    } else if( x[i] == '\r' ) {
      escaped.push_back( '\\' );
      escaped.push_back( 'r' );
    } else if( x[i] == '\t' ) {
      escaped.push_back( '\\' );
      escaped.push_back( 't' );
    } else if( x[i] == '"' ) {
      escaped.push_back( '\\' );
      escaped.push_back( '"' );
    } else {
      escaped.push_back( x[i] );
    }
  }
  escaped.push_back( '"' );
  o << escaped;
}

template<typename T>
void out( std::ostream& o, const T& x ) {
  o << x;
}


} // namespace logging
} // namespace munzekonza

#define DUMPEIGEN(x) \
std::cout << __FILE__ << ":" << __LINE__ << ": " << #x << " is: " << \
    std::endl << (x) << std::endl;

#define DUMPBEENHERE() \
std::cout << __FILE__ << ":" << __LINE__ << ": been here." << std::endl;

#define DUMP1(el0) \
std::cout << __FILE__ << ":" << __LINE__ << ": " << #el0 << " is "; \
munzekonza::logging::out(std::cout, el0);\
std::cout << std::endl;

#define DUMP2(el0, el1) \
std::cout << __FILE__ << ":" << __LINE__ << ": " << #el0 << " is "; \
munzekonza::logging::out(std::cout, el0);\
std::cout << ", " << #el1 << " is "; \
munzekonza::logging::out(std::cout, el1);\
std::cout << std::endl;

#define DUMP3(el0, el1, el2) \
std::cout << __FILE__ << ":" << __LINE__ << ": " << #el0 << " is "; \
munzekonza::logging::out(std::cout, el0);\
std::cout << ", " << #el1 << " is "; \
munzekonza::logging::out(std::cout, el1);\
std::cout << ", " << #el2 << " is "; \
munzekonza::logging::out(std::cout, el2);\
std::cout << std::endl;

#define DUMP4(el0, el1, el2, el3) \
std::cout << __FILE__ << ":" << __LINE__ << ": " << #el0 << " is "; \
munzekonza::logging::out(std::cout, el0);\
std::cout << ", " << #el1 << " is "; \
munzekonza::logging::out(std::cout, el1);\
std::cout << ", " << #el2 << " is "; \
munzekonza::logging::out(std::cout, el2);\
std::cout << ", " << #el3 << " is "; \
munzekonza::logging::out(std::cout, el3);\
std::cout << std::endl;

#define DUMP5(el0, el1, el2, el3, el4) \
std::cout << __FILE__ << ":" << __LINE__ << ": " << #el0 << " is "; \
munzekonza::logging::out(std::cout, el0);\
std::cout << ", " << #el1 << " is "; \
munzekonza::logging::out(std::cout, el1);\
std::cout << ", " << #el2 << " is "; \
munzekonza::logging::out(std::cout, el2);\
std::cout << ", " << #el3 << " is "; \
munzekonza::logging::out(std::cout, el3);\
std::cout << ", " << #el4 << " is "; \
munzekonza::logging::out(std::cout, el4);\
std::cout << std::endl;

#define DUMP6(el0, el1, el2, el3, el4, el5) \
std::cout << __FILE__ << ":" << __LINE__ << ": " << #el0 << " is "; \
munzekonza::logging::out(std::cout, el0);\
std::cout << ", " << #el1 << " is "; \
munzekonza::logging::out(std::cout, el1);\
std::cout << ", " << #el2 << " is "; \
munzekonza::logging::out(std::cout, el2);\
std::cout << ", " << #el3 << " is "; \
munzekonza::logging::out(std::cout, el3);\
std::cout << ", " << #el4 << " is "; \
munzekonza::logging::out(std::cout, el4);\
std::cout << ", " << #el5 << " is "; \
munzekonza::logging::out(std::cout, el5);\
std::cout << std::endl;

#define DUMP7(el0, el1, el2, el3, el4, el5, el6) \
std::cout << __FILE__ << ":" << __LINE__ << ": " << #el0 << " is "; \
munzekonza::logging::out(std::cout, el0);\
std::cout << ", " << #el1 << " is "; \
munzekonza::logging::out(std::cout, el1);\
std::cout << ", " << #el2 << " is "; \
munzekonza::logging::out(std::cout, el2);\
std::cout << ", " << #el3 << " is "; \
munzekonza::logging::out(std::cout, el3);\
std::cout << ", " << #el4 << " is "; \
munzekonza::logging::out(std::cout, el4);\
std::cout << ", " << #el5 << " is "; \
munzekonza::logging::out(std::cout, el5);\
std::cout << ", " << #el6 << " is "; \
munzekonza::logging::out(std::cout, el6);\
std::cout << std::endl;

// variadic DUMP macro
#define GET_8TH_ARG(el0, el1, el2, el3, el4, el5, el6, FUNC, ...) FUNC
#define DUMP_SELECTOR(...) \
  GET_8TH_ARG(__VA_ARGS__, DUMP7, DUMP6, DUMP5, DUMP4, DUMP3, DUMP2, DUMP1, )

#define DUMP(...) DUMP_SELECTOR(__VA_ARGS__)(__VA_ARGS__)


#endif // MUNZEKONZA_LOGGING_DUMP_HPP_
