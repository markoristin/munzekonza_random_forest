#ifndef MUNZEKONZA__DEBUGGING__ASSERT_HPP_
#define MUNZEKONZA__DEBUGGING__ASSERT_HPP_

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <map>

namespace munzekonza {

namespace assert_ns {
template <typename K, typename CollectionT>
bool contains( const CollectionT& haystack, const K& needle ) {
  return  std::find( haystack.begin(), haystack.end(), needle ) != haystack.end();
}

template <typename K, typename V>
bool contains( const std::map<K, V>& haystack, const K& needle ) {
  return haystack.find( needle ) != haystack.end();
}
} // namespace assert_ns

class Assertion_exception : public std::exception {
public:
  Assertion_exception( const std::string& what_msg ) :
    what_msg_( what_msg ) {}

  virtual const char* what() const throw() {
    return  what_msg_.c_str();
  }

  virtual ~Assertion_exception() throw() {}

private:
  const std::string what_msg_;
};

#define ASSERT(x) \
if(!(x)) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
      << #x << std::endl; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define ASSERT_NEQ(x,y) \
if( (x) == (y) ) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
    << #x << " (== " << (x) << ") must be unequal " \
    << #y << " (== " << (y) << ").\n"; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define ASSERT_EQ(x,y) \
if(!( (x) == (y) )) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
    << #x << " (== " << (x) << ") must be equal " \
    << #y << " (== " << (y) << ").\n"; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define ASSERT_EQ_ABSPRECISION(x,y,precision) \
if( abs( (x) - (y) ) > (precision) ) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
      << #x << " (== " << (x) << ") must be equal " \
      << #y << " (== " << (y) << ") " \
      << "within absolute precision " << (precision) << "; " \
      << "( abs( " << #x << " - " << #y << " ) == " \
      << abs( (x) - (y) ) << " ).\n"; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define ASSERT_EQ_REL(x,y,precision) \
if( abs( (x) - (y) ) / abs( (x) ) > (precision) ) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
      << #x << " (== " << (x) << ") must be equal " \
      << #y << " (== " << (y) << ") within " \
      << "relative precision " << (precision) << "; " \
      << "abs( " << #x << " - " << #y << " ) / abs( " << #x << " ) == " \
      << abs( (x) - (y) ) / abs( (x) ) << "\n"; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define ASSERT_LT(x,y) \
if( (x) >= (y) ) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
    << #x << " (== " << (x) << ") must be less than " \
    << #y << " (== " << (y) << ").\n"; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define ASSERT_LE(x,y) \
if( !( (x) <= (y) ) ) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
    << #x << " (== " << (x) << ") must be less-equal " \
    << #y << " (== " << (y) << ").\n"; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define ASSERT_GT(x,y) \
if( !((x) > (y)) ) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
    << #x << " (== " << (x) << ") must be greater than " \
    << #y << " (== " << (y) << ").\n"; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define ASSERT_GE(x,y) \
if( !((x) >= (y)) ) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
    << #x << " (== " << (x) <<") must be greater-equal " \
    << #y << " (== " << (y) << ").\n"; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define ASSERT_CONTAINS(container,element) \
if( !munzekonza::assert_ns::contains(container, element) ) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
        << #element << " ( == " << (element) \
        << ") is not contained in " \
        << #container << "." << std::endl; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define ASSERT_HAS(container,element) \
if( !munzekonza::assert_ns::contains(container, element) ) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
        << #element << " ( == " << (element) \
        << ") is not contained in " \
        << #container << "." << std::endl; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define ASSERT_INDEX(container, index) \
if( (index) < 0 || (index) >= (container).size()) { \
  std::stringstream ss; \
  ss << __FILE__ << ":" << __LINE__ << ": Assertion failed: " \
    << "Index " << (index) << " is out of bound of the container " \
    << #container << " (0, " << (container).size() << ")."; \
  throw munzekonza::Assertion_exception(ss.str()); \
}

#define HAS_MEM_FUNC(func, name)                                        \
    template<typename T, typename Sign>                                 \
    struct name {                                                       \
        typedef char yes[1];                                            \
        typedef char no [2];                                            \
        template <typename U, U> struct type_check;                     \
        template <typename _1> static yes &chk(type_check<Sign, &_1::func> *); \
        template <typename   > static no  &chk(...);                    \
        static bool const value = sizeof(chk<T>(0)) == sizeof(yes);     \
    }

}

#endif // ASSERT_H