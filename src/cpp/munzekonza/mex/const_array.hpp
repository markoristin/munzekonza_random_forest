//
// File:          cpp/munzekonza/mex/const_array.hpp
// Author:        Marko Ristin
// Creation date: May 23 2015
//

#ifndef CPP_MUNZEKONZA_MEX_CONST_ARRAY_HPP_
#define CPP_MUNZEKONZA_MEX_CONST_ARRAY_HPP_

#include "munzekonza/mex/base_array.hpp"

namespace munzekonza {
namespace mex {

/// wrapper around mxArray
class Const_array : public Base_array {
public:
  Const_array( const mxArray* mx_array );

  const int* int_data() const;
  const float* single_data() const;
  const double* double_data() const;

  const mxArray* get() const;

  Eigen::Map <const Eigen::MatrixXf> to_single_matrix_map() const;
  Eigen::Map <const Eigen::MatrixXd> to_double_matrix_map() const;
};
} // namespace mex
} // namespace munzekonza

#endif // CPP_MUNZEKONZA_MEX_CONST_ARRAY_HPP_
