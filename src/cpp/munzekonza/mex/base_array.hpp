//
// File:          munzekonza/mex/base_array.hpp
// Author:        Marko Ristin
// Creation date: May 23 2015
//

#ifndef MUNZEKONZA_MEX_BASE_ARRAY_HPP_
#define MUNZEKONZA_MEX_BASE_ARRAY_HPP_

#include <Eigen/Core>
#include <Eigen/Dense>

#include <vector>
#include <string>

// see extern/include/matrix.h from Matlab directory
typedef struct mxArray_tag mxArray;

namespace munzekonza {

namespace mex {
class Struct;

class Base_array {
public:
  Base_array( const mxArray* mx_array );

  /// \returns true if number of dimensions is equal 2 and it's a numeric
  bool is_matrix() const;

  bool is_int() const;
  bool is_uint64() const;
  bool is_single() const;
  bool is_double() const;

  bool is_complex() const;

  /// \returns true if all dimensions are 1 and it's a numeric
  bool is_scalar() const;

  /// \returns true if 1) one of the two dimensions is 1,
  /// 2) the other at least 1 and 3) it's numeric
  bool is_vector() const;

  bool is_char() const;
  bool is_struct() const;

  int nrows() const;
  int ncols() const;
  int length() const;

  double to_scalar() const;

  /// the values are casted to int.
  void to_vector( std::vector<int>& vec ) const;

  /// the values are casted to float.
  void to_vector( std::vector<float>& vec ) const;

  /// the values are casted to double.
  void to_vector( std::vector<double>& vec ) const;

  std::string to_string() const;

  Struct to_struct() const;

protected:
  const mxArray* const_mx_array_;

  const int* int_data_impl() const;
  const float* single_data_impl() const;
  const double* double_data_impl() const;

private:
  template<typename T>
  void to_vector_impl( std::vector<T>& vec ) const;
};

} // namespace mex
} // namespace munzekonza

#endif // MUNZEKONZA_MEX_BASE_ARRAY_HPP_
