//
// File:          munzekonza/mex/mexout.hpp
// Author:        Marko Ristin
// Creation date: May 06 2015
//

#ifndef MUNZEKONZA_MEX_MEXOUT_HPP_
#define MUNZEKONZA_MEX_MEXOUT_HPP_

#include <sstream>

namespace munzekonza {
class Mexout : public std::streambuf {
public:
protected:
  virtual std::streamsize xsputn( const char *s, std::streamsize n );
  virtual int overflow( int c = EOF );
};

} // namespace munzekonza

#endif // MUNZEKONZA_MEX_MEXOUT_HPP_
