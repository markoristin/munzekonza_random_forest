//
// File:          munzekonza/mex/mexout.cpp
// Author:        Marko Ristin
// Creation date: May 06 2015
//

#include "munzekonza/mex/mexout.hpp"

#include <mex.h>

namespace munzekonza {
std::streamsize Mexout::xsputn( const char *s, std::streamsize n ) {
  mexPrintf( "%.*s", n, s );
  return n;
}

int Mexout::overflow( int c ) {
  if ( c != EOF ) {
    mexPrintf( "%.1s", &c );
  }
  return 1;
}
} // namespace munzekonza
