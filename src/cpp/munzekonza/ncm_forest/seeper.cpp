//
// File:          cpp/class_breakup/ncm_forest/seeping.cpp
// Author:        Marko Ristin
// Creation date: Jul 08 2014
//

#include "seeper.hpp"
#include <munzekonza/random_forest/node.hpp>

#include <munzekonza/debugging/assert.hpp>

namespace rf = munzekonza::random_forest;

namespace munzekonza {

namespace ncm_forest {

typedef Seeper::NodeD NodeD;

void Seeper::set_data( const MatrixD& descriptors ) {
  descriptors_ = &descriptors;
}

void Seeper::set_splits( const std::vector<Split*>& splits ) {
  splits_ = &splits;
}

const NodeD* Seeper::next_node(
  const NodeD* node,
  int sample_id ) const {

  ASSERT( !node->is_leaf() );

  const int direction = splits_->at( node->id )->direction( descriptors_->row( sample_id ) );
  ncomparisons_ += splits_->at( node->id )->directions.size();

  return node->children.at( direction );
}

const NodeD* Seeper::final_node( const NodeD* root, int sample_id ) const {
  ncomparisons_ = 0;
  return rf::Seeper_abstract::final_node( root, sample_id );
}

void Seeper::path(
  const NodeD* root,
  int sample_id,
  std::vector<const NodeD*>& result ) const {

  ncomparisons_ = 0;
  rf::Seeper_abstract::path( root, sample_id, result );
}


void Seeper::get_path(
  NodeD* root,
  int sample_id,
  std::vector<NodeD*>& result ) const {

  ncomparisons_ = 0;
  rf::Seeper_abstract::get_path( root, sample_id, result );
}

int Seeper::ncomparisons() const {
  return ncomparisons_;
}
} // namespace ncm_forest
} // namespace munzekonza 


