//
// File:          munzekonza/ncm_forest/procedures/populate_leaf_stats_of_ncm_tree.cpp
// Author:        Marko Ristin
// Creation date: May 23 2015
//

#include "munzekonza/mex/mexout.hpp"
#include "munzekonza/mex/const_array.hpp"
#include "munzekonza/mex/array.hpp"

#include "munzekonza/ncm_forest/split.hpp"
#include "munzekonza/ncm_forest/seeper.hpp"
#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/random_forest/leaf_stats.hpp"
#include "munzekonza/random_forest/leaf_stats_collector.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include <munzekonza/utils/foreach.hpp>
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE

#include "third_party/matlab_class_wrapper/class_handle.hpp"

#include <mex.h>

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <set>

namespace rf = munzekonza::random_forest;

typedef munzekonza::ncm_forest::Split SplitD;
typedef munzekonza::ncm_forest::Seeper SeeperD;

/// can throw exceptions
void main_function( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  munzekonza::Mexout mexout;
  std::cout.rdbuf( &mexout );

  if( nlhs != 1 ) {
    THROW( "Invalid number of left-hand side arguments. "
           "Usage: leaf_stats = munzekonza.populate_leaf_stats_of_ncm_tree(tree, splits, data, labels, sample_ids)" );
  }

  if( nrhs != 5 ) {
    THROW( "Invalid number of right-hand side arguments. "
           "Usage: leaf_stats = munzekonza.populate_leaf_stats_of_ncm_tree(tree, splits, data, labels, sample_ids)" );
  }

  munzekonza::mex::Const_array tree_mx( prhs[0] );
  munzekonza::mex::Const_array splits_mx( prhs[1] );
  munzekonza::mex::Const_array data_mx( prhs[2] );
  munzekonza::mex::Const_array labels_mx( prhs[3] );
  munzekonza::mex::Const_array sample_ids_mx( prhs[4] );

  class_handle<rf::Node>* tree_handle = convertMat2HandlePtr<rf::Node>( tree_mx.get() );
  class_handle<std::vector<SplitD*> >* splits_handle = convertMat2HandlePtr<std::vector<SplitD*> > ( splits_mx.get() );

  if( !tree_mx.is_scalar() || !tree_mx.is_uint64() || tree_mx.is_complex() || !tree_handle->isValid() )
    THROW( "The argument 'tree' must be a valid handle returned by train_ncm_tree(...)." );

  if( !splits_mx.is_scalar() || !splits_mx.is_uint64() || splits_mx.is_complex() || !splits_handle->isValid() )
    THROW( "The argument 'splits' must be a valid handle returned by train_ncm_tree(...)." );

  if( !data_mx.is_matrix() )       THROW ( "data must be given as a matrix." );
  if( !data_mx.is_single() )       THROW ( "data matrix must be a single-precision matrix. "
        "You can convert with data_single = single(data) if data is a double-precision matrix." );
  if( !labels_mx.is_vector() )     THROW ( "labels must be given as a numeric vector." );
  if( !sample_ids_mx.is_vector() ) THROW ( "sample_ids must be given as a numeric vector." );

  rf::Node* root = tree_handle->ptr();
  std::vector<SplitD*>* splits = splits_handle->ptr();

  const bool verbose = false;

  if( verbose ) SAY( "Retrieving labels from Matlab array..." );
  std::vector<int> labels;
  labels_mx.to_vector( labels );

  if( verbose ) SAY( "Retrieving sample_ids from Matlab array..." );
  std::vector<int> sample_ids;
  sample_ids_mx.to_vector( sample_ids );
  const int nsamples = labels.size();

  foreach_( int sample_id, sample_ids ) {
    ASSERT_LT( sample_id, nsamples );
    ASSERT_GE( sample_id, 0 );
  }

  std::set<int> label_set( labels.begin(), labels.end() );

  ASSERT( !label_set.empty() );
  const int min_label = *label_set.begin();
  const int max_label = *label_set.rbegin();

  if( verbose ) SAY( "Size of data: %d x %d (nsamples x nfeatures)", data_mx.nrows(), data_mx.ncols() );
  if( verbose ) SAY( "Length of labels: %d, total number of classes: %d, min. class id: %d, max. class id: %d",
                       labels_mx.length(), label_set.size(), min_label, max_label );

  if( data_mx.nrows() != labels_mx.length() ) {
    THROW( "Data rows (== %d) must equal labels length (== %d).", data_mx.nrows(), labels_mx.length() );
  }

  if( verbose ) SAY( "Converting data from Matlab's column major format to row major format..." );
  SplitD::MatrixD data( data_mx.to_single_matrix_map() );

  ASSERT( root->count_split_nodes() == splits->size() );
  if( verbose ) SAY( "Populating leaf stats of a tree with %d nodes (%d inner nodes and %d leaves) with %d samples...",
                       root->count_nodes(), root->count_split_nodes(), root->count_leaves(), sample_ids.size() );

  std::vector<rf::Leaf_stat> leaf_stats;

  SeeperD seeper;
  seeper.set_splits( *splits );
  seeper.set_data( data );

  rf::Leaf_stats_collector leaf_stats_collector;
  leaf_stats_collector.collect( root, seeper, sample_ids, labels, leaf_stats );

  int nunpopulated_leaves = 0;
  foreach_( const rf::Leaf_stat & leaf_stat, leaf_stats ) {
    nunpopulated_leaves += leaf_stat.probs.empty();
  }

  if( nunpopulated_leaves > 0 ) {
    THROW( "There are %d unpopulated leaves. Are you populating the leaves with the "
           "same data that you used to train the tree?",
           nunpopulated_leaves );
  }

  if( verbose ) SAY( "Converting leaf stats to Matlab matrix..." );

  munzekonza::mex::Array leaf_stats_mx( mxCreateDoubleMatrix( leaf_stats.size(), max_label + 1, mxREAL ) );
  ASSERT( leaf_stats_mx.get() != NULL );

  Eigen::Map<Eigen::MatrixXd> leaf_stats_matrix( leaf_stats_mx.double_data(), leaf_stats_mx.nrows(), leaf_stats_mx.ncols() );

  foreach_enumerated( int leaf_id, const rf::Leaf_stat & leaf_stat, leaf_stats ) {
    foreach_in_map( int label, double prob, leaf_stat.probs ) {
      leaf_stats_matrix( leaf_id, label ) = prob;
    }
  }

  plhs[0] = leaf_stats_mx.get();
}

/// catches all exceptions
void mexFunction( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  try {
    main_function( nlhs, plhs, nrhs, prhs );
  } catch( std::exception& e ) {
    mexErrMsgTxt( e.what() );
  }
}
