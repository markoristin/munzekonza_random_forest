//
// File:          munzekonza/ncm_forest/procedures/destroy_tree.cpp
// Author:        Marko Ristin
// Creation date: May 20 2015
//

#include "munzekonza/mex/mexout.hpp"
#include "munzekonza/mex/const_array.hpp"
#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/logging/logging.hpp"

#include "third_party/matlab_class_wrapper/class_handle.hpp"

#include <mex.h>

namespace rf = munzekonza::random_forest;

/// can throw exceptions
void main_function( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  munzekonza::Mexout mexout;
  std::cout.rdbuf( &mexout );

  if( nlhs != 0 ) {
    THROW( "Invalid number of left-hand side arguments. "
           "Usage: munzekonza.destroy_tree(tree)" );
  }

  if( nrhs != 1 ) {
    THROW( "Invalid number of right-hand side arguments. "
           "Usage: munzekonza.destroy_tree(tree)" );
  }

  typedef rf::Node Morituri_type;

  munzekonza::mex::Const_array tree_mx(prhs[0]);
  class_handle<Morituri_type>* tree_handle(
    convertMat2HandlePtr<Morituri_type>( tree_mx.get() ));

  if( !tree_mx.is_scalar() || !tree_mx.is_uint64() || tree_mx.is_complex() || !tree_handle->isValid() )
    THROW( "The argument 'tree' must be a valid handle returned by train_ncm_tree(...)." );

  destroyObject<Morituri_type>(tree_mx.get());
}


/// catches all exceptions
void mexFunction( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  try {
    main_function( nlhs, plhs, nrhs, prhs );
  } catch( std::exception& e ) {
    mexErrMsgTxt( e.what() );
  }
}
