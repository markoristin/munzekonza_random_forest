//
// File:          munzekonza/ncm_forest/procedures/train_tree.cpp
// Author:        Marko Ristin
// Creation date: Apr 30 2015
//

#include "munzekonza/mex/mexout.hpp"
#include "munzekonza/mex/const_array.hpp"
#include "munzekonza/mex/struct.hpp"

#include "munzekonza/ncm_forest/training.hpp"
#include "munzekonza/ncm_forest/training_cvpr14.hpp"
#include "munzekonza/ncm_forest/training_regula.hpp"

#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/random_forest/cast_splits.hpp"
#include "munzekonza/random_forest/tree_training.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"

#include "third_party/matlab_class_wrapper/class_handle.hpp"

#include <mex.h>

#include <boost/random/mersenne_twister.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <set>

namespace rf = munzekonza::random_forest;
typedef munzekonza::ncm_forest::Split SplitD;

/// can throw exceptions
void main_function( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  munzekonza::Mexout mexout;
  std::cout.rdbuf( &mexout );

  if( nlhs != 2 ) {
    THROW( "Invalid number of left-hand side arguments. "
           "Usage: [tree, splits] = munzekonza.train_ncm_tree(data, labels, sample_ids, seed, params)" );
  }

  if( nrhs != 5 ) {
    THROW( "Invalid number of right-hand side arguments. "
           "Usage: [tree, splits] = munzekonza.train_ncm_tree(data, labels, sample_ids, seed, params)" );
  }

  munzekonza::mex::Const_array data_mx( prhs[0] );
  munzekonza::mex::Const_array labels_mx( prhs[1] );
  munzekonza::mex::Const_array sample_ids_mx( prhs[2] );
  munzekonza::mex::Const_array seed_mx( prhs[3] );
  munzekonza::mex::Const_array params_mx( prhs[4] );

  if( !data_mx.is_matrix() )       THROW ( "data must be given as a matrix." );
  if( !data_mx.is_single() )       THROW ( "data matrix must be a single-precision matrix. "
        "You can convert with data_single = single(data) if data is a double-precision matrix." );
  if( !labels_mx.is_vector() )     THROW ( "labels must be given as a numeric vector." );
  if( !sample_ids_mx.is_vector() ) THROW ( "sample_ids must be given as a numeric vector." );
  if( !seed_mx.is_scalar() )       THROW ( "seed must be a numeric scalar." );
  if( !params_mx.is_struct() )     THROW ( "params must be given as a struct." );

  munzekonza::mex::Struct params( params_mx.to_struct() );
  const bool verbose = params.get_field("verbose").to_scalar() > 0.0;

  const int seed = seed_mx.to_scalar();
  if(verbose) SAY( "Initializing random number generator with seed %d...", seed );
  boost::mt19937 rng;
  rng.seed( seed );

  if(verbose) SAY( "Retrieving labels from Matlab array..." );
  std::vector<int> labels;
  labels_mx.to_vector( labels );

  if(verbose) SAY( "Retrieving sample_ids from Matlab array..." );
  std::vector<int> sample_ids;
  sample_ids_mx.to_vector( sample_ids );
  const int nsamples = labels.size();

  foreach_( int sample_id, sample_ids ) {
    ASSERT_LT( sample_id, nsamples );
    ASSERT_GE( sample_id, 0 );
  }

  std::set<int> label_set( labels.begin(), labels.end() );

  if(verbose) SAY( "Size of data: %d x %d (nsamples x nfeatures)", data_mx.nrows(), data_mx.ncols() );
  if(verbose) SAY( "Length of labels: %d, total number of classes: %d", labels_mx.length(), label_set.size() );

  if( data_mx.nrows() != labels_mx.length() ) {
    THROW( "Data rows (== %d) must equal labels length (== %d).", data_mx.nrows(), labels_mx.length() );
  }

  if(verbose) SAY( "Converting data from Matlab's column major format to row major format..." );
  SplitD::MatrixD data(data_mx.to_single_matrix_map());

  // produce training
  boost::scoped_ptr<munzekonza::ncm_forest::Training> training_base;
  const std::string training_name = params.get_field( "training" ).to_string();

  if( training_name == "cvpr14" ) {
    munzekonza::ncm_forest::Training_cvpr14* training(
      new munzekonza::ncm_forest::Training_cvpr14() );

    training->set_ncentroids_limit    ( params.get_field ( "ncentroids_limit"    ).to_scalar ()  ) ;
    training->set_min_samples_at_node ( params.get_field ( "min_samples_at_node" ).to_scalar ()  ) ;
    training->set_ntests              ( params.get_field ( "ntests"              ).to_scalar ()  ) ;

    training_base.reset( training );

  } else if( training_name == "cvpr15" ) {
    munzekonza::ncm_forest::Training_regula* training(
      new munzekonza::ncm_forest::Training_regula() );

    training->set_ncentroids_limit    ( params.get_field ( "ncentroids_limit"    ).to_scalar ()  ) ;
    training->set_min_samples_at_node ( params.get_field ( "min_samples_at_node" ).to_scalar ()  ) ;
    training->set_ncentroid_choices   ( params.get_field ( "ncentroid_choices"   ).to_scalar ()  ) ;
    training->set_nassignments        ( params.get_field ( "nassignments"        ).to_scalar ()  ) ;
    training->set_ncentroids_weight   ( params.get_field ( "ncentroids_weight"   ).to_scalar ()  ) ;

    training_base.reset( training );

  } else {
    throw std::runtime_error( "Unhandled training: '" + training_name + "'" );
  }

  training_base->set_rng( rng );
  training_base->set_data( data, labels );

  // do the twist
  rf::Node* root = new rf::Node( 0 );

  // two views of the splits
  std::vector<SplitD*>* splits = new std::vector<SplitD*>();
  std::map<rf::Node*, rf::Split*> split_map;

  if(verbose) SAY( "Initializing tree training..." );
  rf::Tree_training tree_training;
  tree_training.set_training( *training_base );
  tree_training.set_split_prototype( SplitD() );
  tree_training.set_silent(!verbose);

  if(verbose) SAY( "Training a tree..." );
  rf::tree_training::Status status;
  tree_training.train( root, sample_ids, split_map, status );

  rf::enumerate_splitting_nodes_breath_first_then_leaves( root );

  if(verbose) SAY( "Split map contains %d splits.", split_map.size() );

  rf::convert_split_map_to_vector( split_map, *splits );

  if(verbose) SAY( "There were %d splitting nodes trained and there are %d splits.",
       status.splitting_nodes_trained, splits->size() );

  plhs[0] = convertPtr2Mat<rf::Node>( root );
  plhs[1] = convertPtr2Mat<std::vector<SplitD*> >( splits );
}

/// catches all exceptions
void mexFunction( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  try {
    main_function( nlhs, plhs, nrhs, prhs );
  } catch( std::exception& e ) {
    mexErrMsgTxt( e.what() );
  }
}
