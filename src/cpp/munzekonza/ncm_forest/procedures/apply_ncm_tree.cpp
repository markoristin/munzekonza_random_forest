//
// File:          munzekonza/ncm_forest/procedures/apply_ncm_tree.cpp
// Author:        Marko Ristin
// Creation date: May 23 2015
//

#include "munzekonza/mex/mexout.hpp"
#include "munzekonza/mex/array.hpp"
#include "munzekonza/mex/const_array.hpp"

#include "munzekonza/ncm_forest/split.hpp"
#include "munzekonza/ncm_forest/seeper.hpp"
#include "munzekonza/random_forest/node.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"

#include "third_party/matlab_class_wrapper/class_handle.hpp"

#include <mex.h>

namespace rf = munzekonza::random_forest;
typedef munzekonza::ncm_forest::Split SplitD;

/// can throw exceptions
void main_function( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  munzekonza::Mexout mexout;
  std::cout.rdbuf( &mexout );

  if( nlhs != 1 ) {
    THROW( "Invalid number of left-hand side arguments. "
           "Usage: leaf_ids = munzekonza.apply_ncm_tree(tree, splits, data)" );
  }

  if( nrhs != 3 ) {
    THROW( "Invalid number of right-hand side arguments. "
           "Usage: leaf_ids = munzekonza.apply_ncm_tree(tree, splits, data)" );
  }

  munzekonza::mex::Const_array tree_mx( prhs[0] );
  munzekonza::mex::Const_array splits_mx( prhs[1] );
  munzekonza::mex::Const_array data_mx( prhs[2] );

  class_handle<const rf::Node>* tree_handle = convertMat2HandlePtr<const rf::Node>( tree_mx.get() );
  class_handle<const std::vector<SplitD*> >* splits_handle = convertMat2HandlePtr<const std::vector<SplitD*> > ( splits_mx.get() );

  if( !tree_mx.is_scalar() || !tree_mx.is_uint64() || tree_mx.is_complex() || !tree_handle->isValid() )
    THROW( "The argument 'tree' must be a valid handle returned by train_ncm_tree(...)." );

  if( !splits_mx.is_scalar() || !splits_mx.is_uint64() || splits_mx.is_complex() || !splits_handle->isValid() )
    THROW( "The argument 'splits' must be a valid handle returned by train_ncm_tree(...)." );

  if( !data_mx.is_matrix() )       THROW ( "data must be given as a matrix." );
  if( !data_mx.is_single() )       THROW ( "data matrix must be a single-precision matrix. "
        "You can convert with data_single = single(data) if data is a double-precision matrix." );

  const bool verbose = false;

  const rf::Node* root = tree_handle->ptr();
  const std::vector<SplitD*>* splits = splits_handle->ptr();

  if( verbose ) SAY( "Size of data: %d x %d (nsamples x nfeatures)", data_mx.nrows(), data_mx.ncols() );

  if( verbose ) SAY( "Converting data from Matlab's column major format to row major format..." );
  SplitD::MatrixD data( data_mx.to_single_matrix_map() );

  if( verbose ) SAY( "Applying a tree with %d nodes (%d inner nodes and %d leaves) and %d splits to %d samples...",
                       root->count_nodes(), root->count_split_nodes(), root->count_leaves(), splits->size(),
                       data.rows() );

  munzekonza::ncm_forest::Seeper seeper;
  seeper.set_splits( *splits );
  seeper.set_data( data );

  const int nsamples = data.rows();

  munzekonza::mex::Array leaf_ids_mx( mxCreateDoubleMatrix( nsamples, 1, mxREAL ) );
  Eigen::Map<Eigen::MatrixXd> leaf_ids( leaf_ids_mx.double_data(), leaf_ids_mx.nrows(), leaf_ids_mx.ncols() );

  for( int sample_id = 0; sample_id < nsamples; ++sample_id ) {
    const rf::Node* node = seeper.final_node( root, sample_id );
    leaf_ids( sample_id ) = node->leaf_id;
  }

  plhs[0] = leaf_ids_mx.get();
}

/// catches all exceptions
void mexFunction( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  try {
    main_function( nlhs, plhs, nrhs, prhs );
  } catch( std::exception& e ) {
    mexErrMsgTxt( e.what() );
  }
}
