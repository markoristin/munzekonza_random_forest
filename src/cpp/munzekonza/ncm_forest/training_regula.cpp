//
// File:          cpp/class_breakup/ncm_forest/training_regula.cpp
// Author:        Marko Ristin
// Creation date: Aug 25 2014
//

#include "training_regula.hpp"
#include "intermediate_result.hpp"
#include "partition_iterator.hpp"
#include "partition_functions.hpp"
#include "partitioner.hpp"

#include <munzekonza/random_forest/entropier.hpp>
#include <munzekonza/random_forest/node.hpp>
#include <munzekonza/random_forest/training_result.hpp>
#include <munzekonza/numeric/greedy_tsp.hpp>
#include <munzekonza/utils/profiler.hpp>
#include <munzekonza/logging/logging.hpp>
#include <munzekonza/debugging/assert.hpp>
#include <munzekonza/numeric/random_subset.hpp>
#include <munzekonza/utils/foreach.hpp>
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE

#include <boost/scoped_ptr.hpp>

#include <map>
#include <cmath>

namespace rf = munzekonza::random_forest;

namespace munzekonza {

namespace ncm_forest {

bool centroid_choice_less( const std::vector<int>& a, const std::vector<int>& b ) {
  const int n = std::min( a.size(), b.size() );

  for( int i = 0; i < n; ++i ) {
    if( a.at( i ) < b.at( i ) ) {
      return true;
    }
  }
  return false;
}

int centroid_choice_distance( const std::vector<int>& a, const std::vector<int>& b ) {
  int distance = 0;

  int i = 0;
  int j = 0;
  while( i < a.size() && j < b.size() ) {
    if( a.at( i ) == b.at( j ) ) {
      ++i;
      ++j;
    } else if( a.at( i ) < b.at( j ) ) {
      ++i;
      ++distance;
    } else if( a.at( i ) > b.at( j ) ) {
      ++j;
      ++distance;
    } else {
      throw std::runtime_error( "wrong" );
    }
  }

  distance += std::max( a.size(), b.size() ) - std::min( a.size(), b.size() );

  return distance;
}

Training_regula::Training_regula() :
  min_samples_at_node_( 10 ),
  ncentroids_limit_( -1 ),
  ncentroid_choices_( -1 ),
  nassignments_( -1 ),
  ncentroids_weight_( NAN ) {
}


void Training_regula::set_min_samples_at_node( int min_samples_at_node ) {
  min_samples_at_node_ = min_samples_at_node;
}

void Training_regula::set_ncentroids_limit( int ncentroids_limit ) {
  ncentroids_limit_ = ncentroids_limit;
}

void Training_regula::set_ncentroid_choices( int ncentroid_choices ) {
  ncentroid_choices_ = ncentroid_choices;
}

void Training_regula::set_nassignments( int nassignments ) {
  nassignments_ = nassignments;
}

void Training_regula::set_ncentroids_weight( double ncentroids_weight ) {
  ncentroids_weight_ = ncentroids_weight;
}

void Training_regula::train(
  rf::Node* node,
  const std::vector<int>& sample_ids,
  rf::Split& base_split,
  rf::Training_result& result ) {

  ASSERT( rng_ != NULL );
  ASSERT( descriptors_ != NULL );
  ASSERT( labels_ != NULL );
  ASSERT_GT( ncentroids_limit_, 0 );
  ASSERT_GT( min_samples_at_node_, 0 );
  ASSERT_GT( ncentroid_choices_, 0 );
  ASSERT_GT( nassignments_, 0 );
  ASSERT( !std::isnan( ncentroids_weight_ ) );

  Split& split = dynamic_cast<Split&>( base_split );

  profiler_.entry();

  const size_t nsamples = sample_ids.size();

  // no training possible if only one class present
  std::map<int, int> label_count;
  std::set<int> label_set;
  foreach_( int sample_id, sample_ids ) {
    const int label = labels_->at( sample_id );
    ++label_count[label];
    label_set.insert( label );
  }
  ASSERT_GT( label_count.size(), 0 );

  if( label_count.size() == 1 ) {
    //DEBUG( "only one label, created a leaf." );
    result.found_optimal_split = false;
    return;
  }

  if( sample_ids.size() < 2 * min_samples_at_node_ ) {
    //DEBUG( "too few samples (%d), created a leaf.", sample_ids.size() );
    result.found_optimal_split = false;
    return;
  }

  //DEBUG( "Training a node at depth %d with %d samples (%d different classes)...", node->depth, nsamples, label_set.size() );

  // classes of the centroids chosen in each sampling
  std::vector< std::vector<int> > centroid_choices;
  centroid_choices.resize( ncentroid_choices_ );

  const int nmax_centroids = std::max( size_t( 2 ), std::min( label_set.size(), size_t( ncentroids_limit_ ) ) );

  for( int centroid_choice_i = 0; centroid_choice_i < ncentroid_choices_; ++centroid_choice_i ) {

    boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rand_ncentroids(
      *rng_, boost::uniform_int<>( 2, nmax_centroids ) );

    const int ncentroids = rand_ncentroids();

    std::vector<int> centroid_choice;
    munzekonza::numeric::random_subset( *rng_, label_set, ncentroids, centroid_choice );
    std::sort( centroid_choice.begin(), centroid_choice.end() );

    centroid_choices.at( centroid_choice_i ) = centroid_choice;
  }

  // TSP on cache misses
  const int ncentroid_choices = centroid_choices.size();

  profiler_.start( "cc_distances" );
  Eigen::MatrixXi cc_distances = Eigen::MatrixXi::Zero( ncentroid_choices, ncentroid_choices );

  foreach_in_range( int i, 0, ncentroid_choices - 1 ) {
    foreach_in_range( int j, i + 1, ncentroid_choices ) {
      cc_distances( i, j ) = centroid_choice_distance( centroid_choices.at( i ), centroid_choices.at( j ) );
    }
  }

  foreach_in_range( int i, 0, ncentroid_choices - 1 ) {
    foreach_in_range( int j, i + 1, ncentroid_choices ) {
      cc_distances( j, i ) = cc_distances( i, j );
    }
  }

  foreach_in_range( int i, 0, ncentroid_choices ) {
    cc_distances( i, i ) = boost::numeric::bounds<int>::highest();
  }

  std::vector<int> path;
  munzekonza::numeric::Greedy_tsp greedy_tsp;
  greedy_tsp.compute( cc_distances, path );
  profiler_.stop( "cc_distances" );

  // compute all centroids that will appear in the computation
  std::set<int> global_centroid_label_set;
  foreach_( const std::vector<int>& label_choice, centroid_choices ) {
    foreach_( int label, label_choice ) {
      global_centroid_label_set.insert( label );
    }
  }

  std::vector<int> global_centroid_labels( global_centroid_label_set.begin(), global_centroid_label_set.end() );

  const int nglobal_centroids = global_centroid_labels.size();

  profiler_.start( "centroids" );
  MatrixD global_centroids;
  ncm_forest::compute_centroids( *descriptors_, *labels_, sample_ids, global_centroid_labels, global_centroids );
  profiler_.stop( "centroids" );

  std::map<int, int> label_to_global_centroid_i;
  foreach_enumerated( int global_centroid_i, int label, global_centroid_label_set ) {
    label_to_global_centroid_i[label] = global_centroid_i;
  }

  // compute distances between all centroids and samples
  profiler_.start( "distances" );

  MatrixD distances = MatrixD::Zero( nsamples, nglobal_centroids );
  for( int sample_i = 0; sample_i < nsamples; ++sample_i ) {
    const int sample_id = sample_ids.at( sample_i );
    for( int global_centroid_i = 0; global_centroid_i < nglobal_centroids; ++global_centroid_i ) {

      const MatrixD::Scalar distance(
        ( global_centroids.row( global_centroid_i ) - descriptors_->row( sample_id ) ).squaredNorm() );

      distances( sample_i, global_centroid_i ) = distance;
    }
  }

  profiler_.stop( "distances" );

  // compute label to label_i
  std::map<int, int> label_to_label_i;
  foreach_enumerated( int label_i, int label, label_set ) {
    label_to_label_i[label] = label_i;
  }

  Intermediate_result intermediate_result;
  intermediate_result.entropy = boost::numeric::bounds<double>::highest();

  // go over centroid subsets
  result.found_optimal_split = false;

  //foreach_enumerated( int centroid_choice_i, const std::vector<int>& centroid_choice, centroid_choices ) {
  foreach_( int centroid_choice_i, path ) {
    profiler_.start( "centroid_choice_init" );
    const std::vector<int>& centroid_choice = centroid_choices.at( centroid_choice_i );

    const int ncentroids = centroid_choice.size();

    std::vector<int> global_centroid_is( ncentroids );
    foreach_enumerated( int centroid_i, int label, centroid_choice ) {
      const int global_centroid_i = label_to_global_centroid_i.at( label );
      global_centroid_is.at( centroid_i ) = global_centroid_i;
    }

    // compute centroid class histos
    Eigen::MatrixXi centroid_class_histos( ncentroids, label_set.size() );
    centroid_class_histos.setZero();
    profiler_.stop( "centroid_choice_init" );

    profiler_.start( "centroid_class_histos" );
    for( int sample_i = 0; sample_i < nsamples; ++sample_i ) {
      const int sample_id = sample_ids.at( sample_i );
      const int label = labels_->at( sample_id );

      int closest_centroid_i = -1;
      MatrixD::Scalar min_distance = boost::numeric::bounds<MatrixD::Scalar>::highest();

      for( int centroid_i = 0; centroid_i < ncentroids; ++centroid_i ) {
        const int global_centroid_i = global_centroid_is.at( centroid_i );

        const MatrixD::Scalar distance = distances( sample_i, global_centroid_i );

        if( distance < min_distance ) {
          min_distance = distance;
          closest_centroid_i = centroid_i;
        }
      }

      ++centroid_class_histos( closest_centroid_i, label_to_label_i.at( label ) );
    }
    profiler_.stop( "centroid_class_histos" );

    // find optimal partition over nassignments
    boost::scoped_ptr<Partition_iterator> partition_iterator;

    profiler_.start( "assignments" );

    const int64_t max_ntests = std::pow( 2, ncentroids );
    if( ncentroids < 63 && max_ntests < int64_t( nassignments_ ) ) {
      partition_iterator.reset( new Partition_iterator_all( ncentroids ) );
    } else {
      partition_iterator.reset( new Partition_iterator_random( ncentroids, nassignments_, *rng_ ) );
    }

    const int nclasses = centroid_class_histos.cols();

    const int class_range_begin = 0;
    const int class_range_end = nclasses;

    Partitioner partitioner;
    partitioner.compute_class_histo( centroid_class_histos );

    rf::Entropier entropier( class_range_begin, class_range_end );
    entropier.set_nsamples( partitioner.nsamples( class_range_begin, class_range_end ) );

    for( partition_iterator->start(); !partition_iterator->done(); partition_iterator->next() ) {

      partitioner.compute_partition_class_histos(
        centroid_class_histos, partition_iterator->partition() );

      entropier.compute_nsamples_at_assignment( partitioner.partition_class_histos );

      const bool sufficient_split(
        entropier.nsamples_at_assignment_0 >= min_samples_at_node_ &&
        entropier.nsamples_at_assignment_1 >= min_samples_at_node_ );

      if( sufficient_split ) {

        const double entropy(
          entropier.compute_split_entropy( partitioner.partition_class_histos )  +
          ncentroids_weight_ * double( ncentroids ) );

        if( entropy < intermediate_result.entropy ) {
          result.found_optimal_split = true;
          intermediate_result.entropy = entropy;
          intermediate_result.centroid_choice_i = centroid_choice_i;
          intermediate_result.partition = partition_iterator->partition();
          intermediate_result.ncentroids = centroid_choice.size();
        }
      }
    }

    profiler_.stop( "assignments" );
  }

  profiler_.start( "finish" );
  // finish the training
  if( result.found_optimal_split ) {
    result.entropy = intermediate_result.entropy;

    const std::vector<int>& centroid_choice(
      centroid_choices.at( intermediate_result.centroid_choice_i ) );

    split.directions = intermediate_result.partition;

    split.centroids = MatrixD::Zero( centroid_choice.size(), global_centroids.cols() );
    foreach_enumerated( int centroid_i, int label, centroid_choice ) {
      split.centroids.row( centroid_i ) = global_centroids.row( label_to_global_centroid_i.at( label ) );
    }

    const int nchildren = 2;
    node->children.reserve( nchildren );
    for( int child_i = 0; child_i < nchildren; ++child_i ) {
      node->children.push_back( new rf::Node( node->depth + 1 ) );
    }

    result.pending_trainings.resize( nchildren );
    for( int child_i = 0; child_i < nchildren; ++child_i ) {
      result.pending_trainings.at( child_i ).node = node->children.at( child_i );
      result.pending_trainings.at( child_i ).sample_ids.reserve( sample_ids.size() / 2 );
    }

    foreach_( int sample_id, sample_ids ) {
      const int direction = split.direction( descriptors_->row( sample_id ) );

      ASSERT_INDEX( result.pending_trainings, direction );
      result.pending_trainings.at( direction ).sample_ids.push_back( sample_id );
    }

  } else {
    //DEBUG( "no optimal split found, created a leaf." );
  }
  profiler_.stop( "finish" );


  profiler_.exit();
}

void Training_regula::on_level_start( bool silent, int depth ) {
  profiler_.reset();
}

void Training_regula::on_level_finished( bool silent, int depth ) {
  if( !silent ) {
    SAY( "Profiling:\n %s", profiler_.analyze() );
  }
}

} // namespace ncm_forest
} // namespace munzekonza 


