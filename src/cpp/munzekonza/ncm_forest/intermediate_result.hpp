//
// File:          cpp/class_breakup/ncm_forest/intermediate_result.hpp
// Author:        Marko Ristin
// Creation date: Aug 14 2014
//

#ifndef CPP_CLASS_BREAKUP_NCM_FOREST_INTERMEDIATE_RESULT_HPP_
#define CPP_CLASS_BREAKUP_NCM_FOREST_INTERMEDIATE_RESULT_HPP_

#include <boost/numeric/conversion/bounds.hpp>

#include <cmath>

namespace munzekonza {

namespace ncm_forest {

struct Intermediate_result {
  double information_gain;
  double entropy;
  int centroid_choice_i;
  std::vector<int> partition;
  int ncentroids;

  Intermediate_result() :
    information_gain( NAN ),
    entropy( NAN ),
    centroid_choice_i( -1 ),
    ncentroids( -1 )
  {}

};

struct Entropy_criterion {
  bool operator()( const Intermediate_result& self, const Intermediate_result& other ) const {
    return  self.entropy < other.entropy;
  }
};

struct Inv_entropy_criterion {
  bool operator()( const Intermediate_result& self, const Intermediate_result& other ) const {
    return  self.entropy > other.entropy;
  }
};

struct Length_criterion {
  bool operator()( const Intermediate_result& self, const Intermediate_result& other ) const {
    return self.ncentroids < other.ncentroids;
  }
};

} // namespace ncm_forest
} // namespace munzekonza 

#endif // CPP_CLASS_BREAKUP_NCM_FOREST_INTERMEDIATE_RESULT_HPP_


