//
// File:          cpp/class_breakup/ncm_forest/partitioner.hpp
// Author:        Marko Ristin
// Creation date: Sep 23 2014
//

#ifndef CPP_CLASS_BREAKUP_NCM_FOREST_PARTITIONER_HPP_
#define CPP_CLASS_BREAKUP_NCM_FOREST_PARTITIONER_HPP_

#include <Eigen/Core>
#include <Eigen/Dense>

#include <vector>

namespace munzekonza {

namespace ncm_forest {
class Partitioner {
public:
  /// set with compute_class_histo()
  Eigen::MatrixXi class_histo;

  /// set with compute_partition_class_histos()
  Eigen::MatrixXi partition_class_histos;

  void compute_class_histo(
    const Eigen::MatrixXi& centroid_class_histos );

  void compute_partition_class_histos(
    const Eigen::MatrixXi& centroid_class_histos,
    const std::vector<int>& partition );

  int nsamples(
    int class_range_begin,
    int class_range_end ) const;
};
} // namespace ncm_forest
} // namespace munzekonza 

#endif // CPP_CLASS_BREAKUP_NCM_FOREST_PARTITIONER_HPP_
