//
// File:          cpp/class_breakup/ncm_forest/seeping.hpp
// Author:        Marko Ristin
// Creation date: Jul 08 2014
//

#ifndef CPP_CLASS_BREAKUP_NCM_FOREST_SEEPING_HPP_
#define CPP_CLASS_BREAKUP_NCM_FOREST_SEEPING_HPP_

#include <munzekonza/random_forest/seeper_abstract.hpp>

#include "split.hpp"

namespace munzekonza {
namespace random_forest {
class Node;
} // namespace random_forest

namespace ncm_forest {
class Seeper : public munzekonza::random_forest::Seeper_abstract {
public:
  typedef Split::MatrixD MatrixD;
  typedef munzekonza::random_forest::Node NodeD;

  Seeper():
    descriptors_( NULL ),
    splits_( NULL )
  {}

  void set_data( const MatrixD& descriptors );
  void set_splits( const std::vector<Split*>& splits );

  virtual const NodeD* next_node(
    const NodeD* node,
    int sample_id ) const;

  virtual const NodeD* final_node(
    const NodeD* root,
    int sample_id ) const;

  virtual void path(
    const NodeD* root,
    int sample_id,
    std::vector<const NodeD*>& result ) const;

  virtual void get_path(
    NodeD* root,
    int sample_id,
    std::vector<NodeD*>& result ) const;

  /// \returns number of comparisons at the last seeping
  int ncomparisons() const;

  virtual ~Seeper() {}

private:
  const MatrixD* descriptors_;
  const std::vector<Split*>* splits_;

  mutable int ncomparisons_;
};
} // namespace ncm_forest
} // namespace munzekonza

#endif // CPP_CLASS_BREAKUP_NCM_FOREST_SEEPING_HPP_


