//
// File:          cpp/class_breakup/ncm_forest/training_cvpr.cpp
// Author:        Marko Ristin
// Creation date: Aug 25 2014
//

#include "training_cvpr14.hpp"
#include "partition_iterator.hpp"
#include "partition_functions.hpp"

#include <munzekonza/random_forest/node.hpp>
#include <munzekonza/random_forest/training_result.hpp>
#include <munzekonza/random_forest/partition_criterions.hpp>
#include <munzekonza/logging/logging.hpp>
#include <munzekonza/debugging/assert.hpp>
#include <munzekonza/numeric/random_subset.hpp>
#include <munzekonza/utils/foreach.hpp>
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include <boost/scoped_ptr.hpp>

#include <map>

namespace rf = munzekonza::random_forest;

namespace munzekonza {

namespace ncm_forest {

void Training_cvpr14::set_min_samples_at_node( int min_samples_at_node ) {
  min_samples_at_node_ = min_samples_at_node;
}

void Training_cvpr14::set_ncentroids_limit( int ncentroids_limit ) {
  ncentroids_limit_ = ncentroids_limit;
}

void Training_cvpr14::set_ntests( int ntests ) {
  ntests_ = ntests;
}

void Training_cvpr14::train(
  rf::Node* node,
  const std::vector<int>& sample_ids,
  rf::Split& split,
  rf::Training_result& result ) {

  ASSERT_GT( ncentroids_limit_, 0 );

  Split& centroid_set = dynamic_cast<Split&>( split );

  const size_t nsamples = sample_ids.size();

  if( nsamples < 2 * min_samples_at_node_ ) {
    //DEBUG( "too few samples (%d, minimum is %d), created a leaf.", nsamples, min_samples_at_node_ );
    result.found_optimal_split = false;
    return;
  }

  // no training possible if only one class present
  std::map<int, int> label_count;
  std::set<int> label_set;
  foreach_( int sample_id, sample_ids ) {
    const int label = labels_->at( sample_id );
    ++label_count[label];
    label_set.insert( label );
  }
  ASSERT_GT( label_count.size(), 0 );

  if( label_count.size() == 1 ) {
    //DEBUG( "only one label, created a leaf." );
    result.found_optimal_split = false;
    return;
  }

  //DEBUG( "Training a node at depth %d with %d samples (%d different classes)...", node->depth, nsamples, label_set.size() );

  const int ncentroids = std::max( size_t( 2 ), std::min( label_set.size(), size_t( ncentroids_limit_ ) ) );

  munzekonza::numeric::random_subset( *rng_, label_set, ncentroids, centroid_set.labels );

  munzekonza::ncm_forest::compute_centroids( *descriptors_, *labels_, sample_ids, centroid_set.labels, centroid_set.centroids );

  // compute centroid class histos
  Eigen::MatrixXi centroid_class_histos( ncentroids, label_set.size() );
  centroid_class_histos.setZero();

  std::map<int, int> label_to_label_i;
  foreach_enumerated( int label_i, int label, label_set ) {
    label_to_label_i[label] = label_i;
  }

  foreach_( int sample_id, sample_ids ) {
    const int label = labels_->at( sample_id );
    const int label_i = label_to_label_i.at( label );

    MatrixD sample = descriptors_->row( sample_id );
    const int closest_centroid_i = centroid_set.closest_centroid( sample );

    ++centroid_class_histos( closest_centroid_i, label_i );
  }

  // find optimal partition
  result.entropy = boost::numeric::bounds<double>::highest();
  result.found_optimal_split = false;

  boost::scoped_ptr<Partition_iterator> partition_iterator;

  const int64_t max_ntests = std::pow( 2, ncentroids );
  if( ncentroids < 63 && max_ntests < int64_t( ntests_ ) ) {
    partition_iterator.reset( new Partition_iterator_all( ncentroids ) );
  } else {
    partition_iterator.reset( new Partition_iterator_random( ncentroids, ntests_, *rng_ ) );
  }

  for( partition_iterator->start(); !partition_iterator->done(); partition_iterator->next() ) {
    const std::vector<int>& partition = partition_iterator->partition();

    Eigen::MatrixXi partition_class_histos;
    compute_partition_class_histos( centroid_class_histos, partition, partition_class_histos );

    const double entropy = rf::compute_entropy( partition_class_histos );

    bool sufficient_split = true;
    for( int assignment_i = 0; assignment_i < 2; ++assignment_i ) {
      const int samples_at_assignment = partition_class_histos.row( assignment_i ).sum();

      sufficient_split = sufficient_split && ( samples_at_assignment >= min_samples_at_node_ );
    }

    if( sufficient_split && entropy < result.entropy ) {
      result.found_optimal_split = true;
      result.entropy = entropy;

      ASSERT_EQ( partition.size(), centroid_set.centroids.rows() );
      centroid_set.directions = partition;
    }
  }

  // finish the training
  if( result.found_optimal_split ) {
    ASSERT_EQ( centroid_set.centroids.rows(), centroid_set.directions.size() );

    const int nchildren = 2;
    node->children.reserve( nchildren );
    for( int child_i = 0; child_i < nchildren; ++child_i ) {
      node->children.push_back( new rf::Node( node->depth + 1 ) );
    }

    result.pending_trainings.resize( nchildren );
    for( int child_i = 0; child_i < nchildren; ++child_i ) {
      result.pending_trainings.at( child_i ).node = node->children.at( child_i );
      result.pending_trainings.at( child_i ).sample_ids.reserve( sample_ids.size() / 2 );
    }

    foreach_( int sample_id, sample_ids ) {
      const int direction = centroid_set.direction( descriptors_->row( sample_id ) );
      result.pending_trainings.at( direction ).sample_ids.push_back( sample_id );
    }

    //DEBUG( "left samples: %d, right samples: %d", result.pending_trainings.at( 0 ).sample_ids.size(), result.pending_trainings.at( 1 ).sample_ids.size() );
    //DEBUG( "entropy: %f, ncentroids: %d at depth %d", result.entropy, centroid_set.centroids.rows(), node->depth );
  } else {
    //DEBUG( "no optimal split found, created a leaf." );
  }
}
} // namespace ncm_forest
} // namespace munzekonza 


