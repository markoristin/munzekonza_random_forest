//
// File:          cpp/class_breakup/ncm_forest/partitioner.cpp
// Author:        Marko Ristin
// Creation date: Sep 23 2014
//

#include "partitioner.hpp"

#include <munzekonza/utils/foreach.hpp>
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE
#include <munzekonza/debugging/assert.hpp>

namespace munzekonza {

namespace ncm_forest {
void Partitioner::compute_class_histo( const Eigen::MatrixXi& centroid_class_histos ) {
  const int ncentroids = centroid_class_histos.rows();
  const int nclasses = centroid_class_histos.cols();

  class_histo.resize( 1, nclasses );
  class_histo.setZero();

  for( int centroid_i = 0; centroid_i < ncentroids; ++centroid_i ) {
    class_histo += centroid_class_histos.row( centroid_i );
  }

  partition_class_histos.resize( 2, nclasses );
}

void Partitioner::compute_partition_class_histos( const Eigen::MatrixXi& centroid_class_histos, const std::vector<int>& partition ) {
  partition_class_histos.row( 0 ).setZero();

  foreach_enumerated( int centroid_i, int assignment, partition ) {
    if( assignment == 0 ) {
      partition_class_histos.row( 0 ) += centroid_class_histos.row( centroid_i );
    }
  }
  partition_class_histos.row( 1 ) = class_histo - partition_class_histos.row( 0 );
}

int Partitioner::nsamples( int class_range_begin, int class_range_end ) const {
  // in case the entropier points to the end of the class histogram,
  // it means it doesn't span any classes at all
  if( class_range_begin == class_range_end ) {
    return 0;
  }

  ASSERT_LT(class_range_begin, class_histo.cols());
  ASSERT_LE(class_range_end, class_histo.cols());

  const int result(
    class_histo.block( 0, class_range_begin, 1, class_range_end - class_range_begin ).sum() );

  return result;
}

} // namespace ncm_forest
} // namespace munzekonza 
