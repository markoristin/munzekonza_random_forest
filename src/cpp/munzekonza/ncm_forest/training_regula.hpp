//
// File:          cpp/class_breakup/ncm_forest/training_regula.hpp
// Author:        Marko Ristin
// Creation date: Aug 25 2014
//

#ifndef CPP_CLASS_BREAKUP_NCM_FOREST_TRAINING_REGULA_HPP_
#define CPP_CLASS_BREAKUP_NCM_FOREST_TRAINING_REGULA_HPP_

#include "split.hpp"
#include "training.hpp"

#include <munzekonza/utils/profiler.hpp>

#include <boost/random/mersenne_twister.hpp>

namespace munzekonza {
class Profiler;

namespace random_forest {
struct Node;
struct Training_result;
} // namespace random_forest

namespace ncm_forest {

/// more regularized training: entropy + ncentroids_weight * ncentroids
class Training_regula : public Training {
public:
  typedef Split::MatrixD MatrixD;

  Training_regula();

  void set_min_samples_at_node( int min_samples_at_node );
  void set_ncentroids_limit( int ncentroids_limit );
  void set_ncentroid_choices( int ncentroid_choices );
  void set_nassignments( int nassignments );
  void set_ncentroids_weight( double ncentroids_weight );

  /// trains the node with the given samples
  virtual void train(
    munzekonza::random_forest::Node* node,
    const std::vector<int>& sample_ids,
    munzekonza::random_forest::Split& split,
    munzekonza::random_forest::Training_result& result );

  virtual void on_level_start(
    bool silent,
    int depth );

  virtual void on_level_finished(
    bool silent,
    int depth );


  virtual ~Training_regula() {}

private:
  int min_samples_at_node_;
  int ncentroids_limit_;
  int ncentroid_choices_;
  int nassignments_;
  double ncentroids_weight_;

  munzekonza::Profiler profiler_;
};

} // namespace ncm_forest
} // namespace munzekonza 

#endif // CPP_CLASS_BREAKUP_NCM_FOREST_TRAINING_REGULA_HPP_


