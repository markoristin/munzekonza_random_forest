//
// File:          cpp/class_breakup/ncm_forest/split.cpp
// Author:        Marko Ristin
// Creation date: Jul 08 2014
//
#include "split.hpp"
#include <munzekonza/debugging/assert.hpp>
#include <munzekonza/logging/logging.hpp>

#include <boost/numeric/conversion/bounds.hpp>

namespace rf = munzekonza::random_forest;

namespace munzekonza {

namespace ncm_forest {
int Split::closest_centroid( const MatrixD& sample ) const {
  const int nfeatures = centroids.cols();
  ASSERT_EQ( sample.rows(), 1 );
  ASSERT_EQ( sample.cols(), nfeatures );
  int closest_i = -1;

  const int ncentroids = centroids.rows();
  ScalarD min_distance = boost::numeric::bounds<float>::highest();
  for( int centroid_i = 0; centroid_i < ncentroids; ++centroid_i ) {
    const ScalarD sq_dist = ( centroids.row( centroid_i ) - sample ).squaredNorm();

    if( sq_dist < min_distance ) {
      min_distance = sq_dist;
      closest_i = centroid_i;
    }
  }

  ASSERT_GT( closest_i, -1 );
  return closest_i;
}


int Split::direction( const MatrixD& sample ) const {
  const int closest_i = closest_centroid( sample );
  ASSERT_INDEX(directions, closest_i);

  return directions.at( closest_i );
}


Split* Split::create_new() const {
  return new Split();
}

} // namespace ncm_forest
} // namespace munzekonza 


