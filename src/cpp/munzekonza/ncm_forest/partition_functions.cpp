//
// File:          cpp/class_breakup/ncm_forest/partition_functions.cpp
// Author:        Marko Ristin
// Creation date: Aug 11 2014
//

#include "partition_functions.hpp"

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/utils/foreach.hpp"
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <set>

namespace munzekonza {

namespace ncm_forest {
void compute_centroid(
  const partition_functions::MatrixD& descriptors,
  const std::vector<int>& labels,
  const std::vector<int>& sample_ids,
  int centroid_label,
  Eigen::Ref<partition_functions::MatrixD> centroid ) {

  typedef partition_functions::MatrixD MatrixD;

  const int nfeatures = descriptors.cols();

  int nsamples_per_centroid = 0;

  centroid.resize( 1, nfeatures );
  centroid.setZero();

  foreach_( int sample_id, sample_ids ) {
    const int label = labels.at( sample_id );
    if( label == centroid_label ) {
      ++nsamples_per_centroid;
      centroid += descriptors.row( sample_id );
    }
  }

  centroid /= static_cast<MatrixD::Scalar>( nsamples_per_centroid );
}

void compute_centroids(
  const partition_functions::MatrixD& descriptors,
  const std::vector<int>& labels,
  const std::vector<int>& sample_ids,
  const std::vector<int>& centroid_labels,
  partition_functions::MatrixD& centroids ) {

  typedef partition_functions::MatrixD MatrixD;

  const int nfeatures = descriptors.cols();
  const int ncentroids = centroid_labels.size();

  ASSERT_GT( nfeatures, 0 );
  ASSERT_GT( ncentroids, 0 );

  std::map<int, int> label_to_centroid_i;
  foreach_enumerated( int centroid_i, int label, centroid_labels ) {
    label_to_centroid_i[label] = centroid_i;
  }

  centroids.resize( ncentroids, nfeatures );
  centroids.setZero();

  std::vector<int> nsamples_per_centroid( ncentroids, 0 );

  foreach_( int sample_id, sample_ids ) {
    const int label = labels.at( sample_id );
    auto it = label_to_centroid_i.find( label );
    if( it == label_to_centroid_i.end() ) {
      continue;
    }
    const int centroid_i = it->second;
    ++nsamples_per_centroid.at( centroid_i );
    centroids.row( centroid_i ) += descriptors.row( sample_id );
  }

  foreach_in_range( int centroid_i, 0, centroids.rows() ) {
    centroids.row( centroid_i ) /= static_cast<MatrixD::Scalar>( nsamples_per_centroid.at( centroid_i ) );
  }
}

void compute_partition_class_histos(
  const Eigen::MatrixXi& centroid_class_histos,
  const std::vector<int>& partition,
  Eigen::MatrixXi& partition_class_histos ) {

  ASSERT_GT( centroid_class_histos.size(), 0 );

  const int nclasses = centroid_class_histos.cols();

  if( partition_class_histos.outerSize() == 0 ) {
    partition_class_histos.derived().resize( 2, nclasses );
  } else {
    ASSERT_EQ( partition_class_histos.rows(), 2 );
    ASSERT_EQ( partition_class_histos.cols(), nclasses );
  }
  partition_class_histos.setZero();

  foreach_enumerated( int centroid_i, int assignment, partition ) {
    partition_class_histos.row( assignment ) += centroid_class_histos.row( centroid_i );
  }
}

double compute_combi_entropy(
  const Eigen::MatrixXi& partition_class_histos,
  double label_weight,
  const std::vector<int>& label_partition ) {
  const int nclasses = partition_class_histos.cols();
  ASSERT_EQ( nclasses, label_partition.size() );

  // one entropy for left and one for right
  std::vector<double> entropies( 2, 0.0 );

  // 0 = # samples left, 1 = # samples right
  std::vector<double> assignment_count = {
    double( partition_class_histos.row( 0 ).sum() ),
    double( partition_class_histos.row( 1 ).sum() )
  };
  const double nsamples = std::accumulate( assignment_count.begin(), assignment_count.end(), 0.0 );
  ASSERT_GT( nsamples, 0.0 );

  double entropy = 0.0;

  for( int i = 0; i < partition_class_histos.rows(); ++i ) {
    const Eigen::VectorXi assignment_histo = partition_class_histos.row( i );

    // one entropy for old labels and one for new labels
    std::vector<double> label_entropies( 2, 0.0 );

    // how many total samples of the old/new labels at this side of the split
    std::vector<double> label_count( 2, 0.0 );
    for( int label_i = 0; label_i < nclasses; ++label_i ) {
      label_count.at( label_partition.at( label_i ) ) += assignment_histo[label_i];
    }

    for( int label_i = 0; label_i < nclasses; ++label_i ) {
      const int label_level = label_partition.at( label_i );
      if( label_count.at( label_level ) > 0 ) {
        const double prob = double( assignment_histo[label_i] ) / label_count.at( label_level );

        if( prob > 0.0 ) {
          label_entropies.at( label_level ) += -prob * std::log( prob );
        }
      }
    }

    entropy += assignment_count.at( i ) / nsamples * ( label_weight * label_entropies.at( 0 ) + label_entropies.at( 1 ) ) ;

    ASSERT( !std::isnan( entropy ) );
  }

  return entropy;
}

} // namespace ncm_forest
} // namespace munzekonza 


