//
// File:          munzekonza/utils/unittest_memory_usage.cpp
// Author:        Marko Ristin
// Creation date: Feb 19 2013
//

#include "munzekonza/utils/memory_usage.hpp"

#include "munzekonza/logging/logging.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>
#include <sys/types.h>
#include <unistd.h>

BOOST_AUTO_TEST_CASE( Test_memory_usage ) {
  std::vector<int> bulk;

  for( int i = 0; i < 10; ++i ) {
    const int mem = i * 1024 * 1024;
    bulk.resize(mem);

    const pid_t pid = getpid();
    system((boost::format("top -bn1 -p %d") % pid).str().c_str());
  }
}

