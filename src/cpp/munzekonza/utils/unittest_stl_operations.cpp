//
// File:          munzekonza/utils/unittest_stl_operations.cpp
// Author:        Marko Ristin
// Creation date: Sep 15 2014
//

#include "munzekonza/utils/stl_operations.hpp"

#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>

BOOST_AUTO_TEST_CASE( Test_sum ) {
  std::vector<int> coll = {1, 2, 3};

  const double mine = munzekonza::sum(coll);
  ASSERT_EQ(mine, 1 + 2 + 3);
}

