#include "munzekonza/utils/tictoc.hpp"

#include <iostream>
#include <cmath>

namespace munzekonza {

Tictoc::Tictoc() : last_lap_( NAN ) {
  tic();
}

void Tictoc::tic() {
  t_ = std::chrono::high_resolution_clock::now();
}

double Tictoc::toc() {
  std::chrono::high_resolution_clock::time_point now(
    std::chrono::high_resolution_clock::now() );

  last_lap_= std::chrono::duration_cast<LapT>( now - t_ );

  return last_lap_.count();
}

double Tictoc::toc( const char* msg ) {
  toc();
  std::cout << msg << " took: " << last_lap_.count() << " seconds." << std::endl;
  return last_lap_.count();
}

double Tictoc::last_lap() const {
  return last_lap_.count();
}


}

