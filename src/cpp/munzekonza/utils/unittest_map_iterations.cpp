//
// File:          munzekonza/utils/unittest_map_iterations.cpp
// Author:        Marko Ristin
// Creation date: Feb 19 2014
//

#include "munzekonza/utils/map_iterations.hpp"
#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <vector>
#include <map>
#include <string>

typedef std::map<int, std::string> MapD;
MapD create_map() {
  std::map<int, std::string> a = { {3, "oi"}, {5, "polloi"}, {2, "noi"}};
  return a;
}


BOOST_AUTO_TEST_CASE( Test_key_iteration ) {
  std::map<int, std::string> a = create_map();
  
  munzekonza::utils::Key_const_iteration<MapD> key_const_iteration(a);

  std::vector<int> keys;
  foreach_( int key, key_const_iteration ) {
    keys.push_back(key);
  }

  std::vector<int> golden_keys;
  for(auto it = a.begin(); it != a.end(); ++it) {
    golden_keys.push_back(it->first);
  }

  ASSERT_EQ(keys.size(), golden_keys.size());

  for( int key_i = 0; key_i < keys.size(); ++key_i) {
    ASSERT_EQ(keys.at(key_i), golden_keys.at(key_i));
  }
}

