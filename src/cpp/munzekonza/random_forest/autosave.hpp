//
// File:          munzekonza/random_forest/autosave.hpp
// Author:        Marko Ristin
// Creation date: Oct 13 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_AUTOSAVE_HPP_
#define MUNZEKONZA_RANDOM_FOREST_AUTOSAVE_HPP_

#include "munzekonza/random_forest/pending_training.hpp"

#include <map>
#include <vector>

namespace munzekonza {
namespace random_forest {

class Node;
class Split;
class Pending_training;

class Autosave {
public:
  virtual void on_training_start() = 0;
  virtual void on_training_finished() = 0;

  /// should call save()
  virtual void on_node_finished(
    Node* root,
    std::map< Node*, Split* >& local_split_map,
    std::map<int, std::list<Pending_training> >& depth_queue ) = 0;

  virtual void save(
    Node* root,
    std::map< Node*, Split* >& local_split_map,
    std::map<int, std::list<Pending_training> >& depth_queue ) = 0;

  /// \returns false if the tree training can not be restored
  bool restore(
    Node* root,
    std::map<Node*, Split*> & local_split_map,
    std::map<int, std::list<Pending_training> >& depth_queue );


  virtual ~Autosave() {}

protected:
  /// only deserializes the objects.
  ///
  /// \remark restore() will take care of fixing the 
  /// root pointer
  ///
  /// \returns false if the file can not be deserialized
  virtual bool deserialize(
    Node*& deserialized_root,
    std::map<Node*, Split*> & deserialized_split_map,
    std::map<int, std::list<Pending_training> >& depth_queue ) = 0;

};

} // namespace random_forest
} // namespace munzekonza


#endif // MUNZEKONZA_RANDOM_FOREST_AUTOSAVE_HPP_
