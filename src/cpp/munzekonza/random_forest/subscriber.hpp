//
// File:          munzekonza/random_forest/subscriber.hpp
// Author:        Marko Ristin
// Creation date: Oct 13 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_SUBSCRIBER_HPP_
#define MUNZEKONZA_RANDOM_FOREST_SUBSCRIBER_HPP_

namespace munzekonza {
namespace random_forest {
class Subscriber {
public:
  virtual void on_level_start() = 0;
  virtual void on_level_finished() = 0;
  virtual void on_node_training_start() = 0;
  virtual void on_node_training_finished() = 0;

  virtual ~Subscriber() {}
};

} // namespace random_forest 
} // namespace munzekonza 

#endif // MUNZEKONZA_RANDOM_FOREST_SUBSCRIBER_HPP_
