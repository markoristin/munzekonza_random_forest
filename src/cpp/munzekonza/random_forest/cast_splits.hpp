//
// File:          ./munzekonza/random_forest/cast_splits.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef __MUNZEKONZA_RANDOM_FOREST_CAST_SPLITS_HPP_
#define __MUNZEKONZA_RANDOM_FOREST_CAST_SPLITS_HPP_

#include <vector>
#include <map>

namespace munzekonza {

namespace random_forest {
class Split;
class Node;

template<typename SplitD>
void downcast_splits(
  const std::vector<SplitD*>& splits,
  std::vector<Split*>& base_splits );

template<typename SplitD>
void upcast_splits(
  const std::vector<Split*>& base_splits,
  std::vector<SplitD*>& splits );

template<typename SplitD>
void convert_split_map_to_vector(
  const std::map<Node*, Split*>& split_map,
  std::vector<SplitD*>& splits );

/// \remark the split map will be extended if it's not empty
template<typename SplitD>
void convert_splits_to_map(
  Node* root,
  const std::vector<SplitD*>& splits,
  std::map<Node*, Split*>& split_map );

} // namespace random_forest
} // namespace munzekonza

#endif // __MUNZEKONZA_RANDOM_FOREST_CAST_SPLITS_HPP_
