//
// File:          munzekonza/random_forest/partition_criterions.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_PARTITION_CRITERIONS_HPP_
#define MUNZEKONZA_RANDOM_FOREST_PARTITION_CRITERIONS_HPP_

#include <Eigen/Core>
#include <Eigen/Dense>

namespace munzekonza {

namespace random_forest {

/// computes the entropy of the histogram to left (row 0) and 
/// right (row 1).
/// 
/// \remark depracted. Use Entropier class.
double compute_entropy(
  Eigen::Ref<Eigen::MatrixXi> partition_class_histos );

} // namespace random_forest
} // namespace munzekonza

#endif // MUNZEKONZA_RANDOM_FOREST_PARTITION_CRITERIONS_HPP_
