//
// File:          munzekonza/random_forest/tree_training.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_TREE_TRAINING_HPP_
#define MUNZEKONZA_RANDOM_FOREST_TREE_TRAINING_HPP_

#include "munzekonza/random_forest/status.hpp"

#include <boost/numeric/conversion/bounds.hpp>
#include <boost/scoped_ptr.hpp>

#include <map>
#include <vector>
#include <list>

namespace munzekonza {

namespace random_forest {
class Split;
class Training;
class Node;
class Subscriber;
class Autosave;

class Tree_training {
public:
  Tree_training() :
    silent_( false ),
    max_depth_( boost::numeric::bounds<int>::highest() ),
    training_(NULL),
    split_prototype_(NULL),
    autosave_(NULL),
    current_depth_(-1),
    current_node_(NULL)
  {}

  void set_max_depth( int max_depth );
  void set_training(Training& training);
  void set_split_prototype(const Split& split_prototype);

  void set_silent( bool silent );
  void add_subscriber(Subscriber& subscriber);
  void remove_subscriber( Subscriber& subscriber );

  void set_autosave(Autosave& autosave);


  /// \remark the caller takes ownership of the splits and the nodes
  virtual void train(
    Node* root,
    const std::vector<int>& sample_ids,
    std::map<Node*, Split*>& split_map,
    tree_training::Status& status
  );

  /// \returns current depth of the tree
  int current_depth() const;

  /// \returns node that is currently trained
  Node* get_current_node();

  /// \returns node that is currently trained
  const Node* current_node() const;

  virtual ~Tree_training() {}

private:
  bool silent_;
  int max_depth_;

  Training* training_;
  boost::scoped_ptr<Split> split_prototype_;
  Autosave* autosave_;

  int current_depth_;
  Node* current_node_;

  std::list<Subscriber*> subscribers_;
};

} // namespace random_forest
} // namespace munzekonza

#endif // MUNZEKONZA_RANDOM_FOREST_TREE_TRAINING_HPP_
