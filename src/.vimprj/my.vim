:set makeprg=make\ train_ncm_tree.mexa64
:map <F5> :make<CR>

:let g:syntastic_cpp_include_dirs = [ '/scratch_net/munzekonza/mristin/apps/opencv-2.4/include', '/scratch_net/munzekonza/mristin/apps/boost-1.55/include', '/scratch_net/munzekonza/mristin/apps/ctemplate-2.2/include', '/scratch_net/munzekonza/mristin/apps/libeigen/include/eigen3', '/scratch_net/munzekonza/mristin/apps/glog/include' ]
let g:syntastic_cpp_compiler_options = ' -std=c++0x'
let g:syntastic_cpp_compiler = 'clang++'
