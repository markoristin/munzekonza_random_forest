function ncm_forest_demo
  % author: Marko Ristin, marko.ristin@gmail.com, May 20 2015
  %
  % performs a demonstration of forest training and testing on the data normalized by whitening.

  % adapt to suit your system
  addpath( '/home/mristin/projects/munzekonza_random_forest/lib' )

  % path to the directory where traning and test data are stored
  data_dir = '/home/mristin/projects/munzekonza_random_forest/data/ilsvrc_2010_10_classes';

  % path where to save the forests
  forest_dir = '/tmp/forest'; 
  if ~exist(forest_dir, 'dir') 
    mkdir(forest_dir); 
  end

  % load data
  train_data_file = load( fullfile( data_dir, 'train_data.mat' ) );
  train_data = train_data_file.train_data;

  train_labels_file = load( fullfile( data_dir, 'train_labels.mat' ) );
  train_labels = train_labels_file.train_labels;

  test_data_file = load( fullfile( data_dir, 'test_data.mat' ) );
  test_data = test_data_file.test_data;

  test_labels_file = load( fullfile( data_dir, 'test_labels.mat' ) );
  test_labels = test_labels_file.test_labels;

  disp( sprintf( 'train_data: %s %d x %d', class( train_data ), size( train_data, 1 ), size( train_data, 2 ) ) );
  disp( sprintf( 'train_labels: %s %d x %d', class( train_labels ), size( train_labels, 1 ), size( train_labels, 2 ) ) );
  disp( sprintf( 'test_data: %s %d x %d', class( test_data ), size( test_data, 1 ), size( test_data, 2 ) ) );
  disp( sprintf( 'test_labels: %s %d x %d', class( test_labels ), size( test_labels, 1 ), size( test_labels, 2 ) ) );

  nclasses = length(unique( train_labels ));
  disp(sprintf('nclasses: %d', nclasses));

  % choose which training you want to perform (see our CVPR'14 and CVPR'15 papers)
  training = 'cvpr14';
  %training = 'cvpr15';

  % set the parameters
  params = struct( );
  if strcmp( training, 'cvpr14' )
    params = struct( 'training', 'cvpr14', ...
                    'min_samples_at_node', 10, ... % \mu: min number of samples at each node and leaf.
                    'ntests', 1025, ... % size of the test pool at each node.
                    'ncentroids_limit', round(sqrt( nclasses )), ... % the max number of class means at a splitting node 
                    'verbose', 0 ... % set verbose = 1 for verbose output.
                    ); 

  elseif strcmp( training, 'cvpr15' )
    params = struct( 'training', 'cvpr15', ...
                    'min_samples_at_node', 10, ... % \mu: min number of samples at each node and leaf.
                    'ncentroid_choices', 1000, ... % number of class mean sets sampled at each node.
                    'nassignments', 50, ... % number of assignments sampled at each node.
                    'ncentroids_weight', 0.001, ... % regularization term \lambda_{reg}.
                    'ncentroids_limit', round(sqrt( nclasses )), ... % the max number of class means at a splitting node 
                    'verbose', 0 ... % set verbose = 1 for verbose output.
                    ); 
  else
    error( sprintf( 'Unknown training type: %s', training ) );
  end


  % compute normalization
  nfeatures = size( train_data, 2 );

  centers = zeros( 1, nfeatures );
  sigmas = zeros( 1, nfeatures );
  for j = 1 : nfeatures
    centers( j ) = mean( train_data( : , j ) );
    sigmas( j ) = std( train_data( : , j ) );
  end

  % normalize train and test data
  ntrain_samples = size( train_data, 1 );
  for i = 1 : ntrain_samples
    train_data( i, : ) = ( train_data( i, : ) - centers ) ./ sigmas;
  end

  ntest_samples = size( test_data, 1 );
  for i = 1 : ntest_samples
    test_data( i, : ) = ( test_data( i, : ) - centers ) ./ sigmas;
  end

  % train a forest
  ntrees = 50;

  for tree_i = 1 : ntrees
    disp( sprintf( 'Training tree %d...', tree_i ) );

    % train with all samples; the index counting starts from 0.
    sample_ids = ( 0 : ( ntrain_samples - 1 ) );

    rng_seed = 22 * 11 * tree_i + 1984;
    [ tree, splits ] = munzekonza.train_ncm_tree( train_data, train_labels, sample_ids, rng_seed, params );
    leaf_stats  = munzekonza.populate_leaf_stats_of_ncm_tree( tree, splits, train_data, train_labels, sample_ids );

    tree_path = fullfile(forest_dir, sprintf('tree_%d.bin', tree_i));
    splits_path = fullfile(forest_dir, sprintf('ncm_splits_%d.bin', tree_i));
    leaf_stats_path = fullfile(forest_dir, sprintf('leaf_stats_%d.mat', tree_i));

    % serialize
    munzekonza.save_tree(tree, tree_path);
    munzekonza.save_ncm_splits(splits, splits_path);
    save(leaf_stats_path, 'leaf_stats');

    % clean up memory
    munzekonza.destroy_tree(tree);
    munzekonza.destroy_ncm_splits(splits);
  end

  % test the forest
  ntest_samples = size( test_data, 1 );
  aggregated_classification = [ ];

  for tree_i = 1 : ntrees
    % deserialize
    tree_path = fullfile(forest_dir, sprintf('tree_%d.bin', tree_i));
    splits_path = fullfile(forest_dir, sprintf('ncm_splits_%d.bin', tree_i));
    leaf_stats_path = fullfile(forest_dir, sprintf('leaf_stats_%d.mat', tree_i));
    
    tree = munzekonza.load_tree(tree_path);
    splits = munzekonza.load_ncm_splits(splits_path);
    leaf_stats_file = load(leaf_stats_path);
    leaf_stats = leaf_stats_file.leaf_stats;

    % apply
    leaf_ids = munzekonza.apply_ncm_tree( tree, splits, test_data );

    % convert c++ indices to matlab indices
    leaf_ids = leaf_ids + 1;

    nleaves = size( leaf_stats, 1 );
    max_class_id = size( leaf_stats, 2 );

    classification = zeros( ntest_samples, max_class_id );

    for sample_i = 1 : ntest_samples
      leaf_id = leaf_ids( sample_i );
      classification( sample_i, : ) = leaf_stats( leaf_id, : );
    end

    if isempty( aggregated_classification )
      aggregated_classification = classification / ntrees;
    else
      aggregated_classification = aggregated_classification + classification / ntrees;
    end

    % clean up memory
    munzekonza.destroy_tree(tree);
    munzekonza.destroy_ncm_splits(splits);
  end

  % pick the most probable class
  estimated = zeros( ntest_samples, 1 );
  for sample_i = 1 : ntest_samples
    [ ~, estimated_class_id ] = max( aggregated_classification( sample_i, : ) );

    % class ids start with 0
    estimated_class_id = estimated_class_id - 1;
    estimated( sample_i ) = estimated_class_id;
  end

  % compute avg. accuracy
  avg_accuracy = sum( test_labels == estimated ) / length( test_labels );

  disp( sprintf( 'Avg. accuracy is %.2f%%', avg_accuracy * 100.0 ) );
end
